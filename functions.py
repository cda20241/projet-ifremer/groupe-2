import csv
from collections import defaultdict

# Constantes
INDEX_COMPTEUR = 0

def extract_csv():
    """Recupère le fichier csv marees_v1.csv, puis le convertie en dictionnaire de liste sous forme de liste
    de liste (matrice), avec les coordonnées correspondantes au X et Y du fichier, et la valeur à Actifs
    Returns:
        dict[list[list]]: renvoi un dict de matrice
    """
    # Dictionnaire pour stocker les données
    donnees_par_date = defaultdict(dict)

    # Ouvre le fichier CSV en mode lecture
    with open("marees_v1.csv", newline='') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=';')

        # Ignore la première ligne (en-têtes)
        next(csvreader, None)

        # Lit chaque ligne du fichier CSV
        for row in csvreader:
            _, x, y, key_date, actif = row
            
            # Convertit X, Y et Actif en entiers
            x, y, actif = int(x), int(y), int(actif)
            
            # Vérifie si la date est déjà une clé dans le dictionnaire
            if key_date not in donnees_par_date:
                donnees_par_date[key_date] = [[0, 0, 0] for _ in range(3)]  # Crée une matrice 3x3 de zéros

            # Met à jour la matrice avec la valeur Actif
            donnees_par_date[key_date][y][x] = actif
    return donnees_par_date

def print_matrice_in_dict(dictionnaire) :
    """prend un dictionnaire de matrice et les affiches avec leur clé
    Args:
        dict (list[list]): dict de matrice
    """
    # Affiche le dictionnaire résultant
    for date, matrice in dictionnaire.items():
        print(f"Key : {date}")
        for ligne in matrice:
            print(ligne)

# Fonction 'max_coefficient_maree'
def max_coefficient_maree(dict_marees):
    """
    La fonction 'max_coefficient_maree' retourne la liste des moments où un maximum de capteurs marins étaient activés.
    Ce qui signifie lorsque la mère était la plus haute d'une période
    :param dict_marees: 'dict' {'date_heure' : [matrice de capteurs]}
    :return: 'list'
    """

    # Comptage des capteurs à chaque moment (clés des dictionnaires)
    nombre_capteurs = []
    for maree in dict_marees.values():
        compteur = INDEX_COMPTEUR
        for x in range(len(maree)):
            for y in range(len(maree[x])):
                compteur += maree[x][y]
        nombre_capteurs.append(compteur)

    # Comparaison du nombre de capteurs à chaque moment (clés des dictionnaires) avec le maximum observé
    liste_dates_heures = []
    for coefficient_maree in range(len(dict_marees)):
        if nombre_capteurs[coefficient_maree] == max(nombre_capteurs):
            liste_dates_heures.append(list(dict_marees.keys())[coefficient_maree])

    return liste_dates_heures

# Fonction 'min_coefficient_maree'
def min_coefficient_maree(dict_marees):
    """
    La fonction 'min_coefficient_maree' retourne la liste des moments où un minimum de capteurs marins étaient activés.
    Ce qui signifie lorsque la mère était la plus basse d'une période.
    :param dict_marees: 'dict' {'date_heure' : [matrice de capteurs]}
    :return: 'list'
    """

    # Comptage des capteurs à chaque moment (clés des dictionnaires)
    nombre_capteurs = []
    for maree in dict_marees.values():
        compteur = INDEX_COMPTEUR
        for x in range(len(maree)):
            for y in range(len(maree[x])):
                compteur += maree[x][y]
        nombre_capteurs.append(compteur)

    # Comparaison du nombre de capteurs à chaque moment (clés des dictionnaires) avec le minimum observé
    liste_dates_heures = []
    for coefficient_maree in range(len(dict_marees)):
        if nombre_capteurs[coefficient_maree] == min(nombre_capteurs):
            liste_dates_heures.append(list(dict_marees.keys())[coefficient_maree])

    return liste_dates_heures

def point_plus_haut(dictionnaire : dict) :
    """Prend un dictionnaire de matrice, cherche ensuite la position (x,y) sur la plage(matrice),
     où la marée a atteint le plus haut point.

    Args:
        dictionnaire (dict[list[list]]): dictionnaire de matrice

    Returns:
        tupple(key, x, y): list composé de la clé, de la position x et y de la position
    """
    plus_haut = None
    # On commence par la dernière ligne X de nos matrice
    for x in range(2, -1, -1) :
        # On vérifie toutes nos matrices
        for key, value in dictionnaire.items() :
            # On recherche les occurences de chaque (x,y)
            for y in range(0, 3) :
                # Si aucune valeur déjà enregistrer, on prend (key,x,y)
                if plus_haut == None :
                    plus_haut = (key, x, y)
                # Si valeur == 1 ou supérieur à notre ref, return le tupple
                elif value[plus_haut[1]][plus_haut[2]] < value[x][y] and value[x][y] == 1 :
                    plus_haut = (key, x, y)
                    return plus_haut

def check_periode(dictionnaire : dict, periode : str) :
    """Verifie si la date de la période est bien présente dans le dictionnaire de matrice.
    Args:
        dictionnaire (dict[list[list]]): dictionnaire de matrice
        periode (str): date du début ou la fin de la période
    Returns:
        Boolean: Return True si présente, False sinon
    """
    for key in dictionnaire.keys() :
        if periode == key :
            return True
    return False

def check_coord(dictionnaire : dict, coord : (int , int )) :
    """Verifie si les coordonnées sont bien présentent dans les matrices.
    Args:
        dictionnaire (dict[list[list]]): dictionnaire de matrice
        coord (int , int): tupple de coordonnées

    Returns:
        Boolean: Return True si présente, False sinon
    """
    for value in dictionnaire.values() :
        if coord[0] <= len(value) and coord[1] <= len(value[0]) :
            return True
    return False

def gimme_periode(dictionnaire : dict, periode: (str, str)) :
    """ Récupère les matrices de la période donnée en paramètre, en return une liste de matrice

    Args:
        dictionnaire (dict[list[list]]): dictionnaire de matrice
        periode (str, str): tupples de string représentant une période en date

    Returns:
        list[list[list]]: liste de matrice
    """
    p_debut = periode[0]
    p_fin = periode[1]
    list_matrice = []
    ind = 0
    for key in dictionnaire.keys() :
        if key == p_fin :
            list_matrice.append(dictionnaire.get(key))
            return list_matrice
        elif key == p_debut or ind == 1 :
            ind = 1
            list_matrice.append(dictionnaire.get(key))

def pourcentage_sous_eau(dictionnaire : dict, coord : (int, int), periode : (str, str)) :
    """On récupère notre période (liste de matrice) sous forme de liste, on parcourt les matrices en incrémentant dans
    un compteur à chaque fois qu'une valeur de (x,y) == 1
    Ensuite on retourne le compteur divisé par le nombre de matrice de notre période

    Args:
        dictionnaire (dict[list[list]]): dictionnaire de matrice
        coord (int, int): coordonnées
        periode (str, str): période

    Raises:
        ValueError: Erreur de coordonnée
        ValueError: Erreur de période

    Returns:
        int: rend un pourcentage sur 100
    """
    if check_periode(dictionnaire, periode[0]) and check_periode(dictionnaire, periode[1]) :
        if check_coord(dictionnaire, coord) :
            coord_x = coord[0]
            coord_y = coord[1]
            list_matrice = gimme_periode(dictionnaire, periode)
            compteur = 0
            for matrice in list_matrice :
                if matrice[coord_x][coord_y] == 1 :
                    compteur += 1
            pourcentage = int((compteur * 100) / len(list_matrice))
            return pourcentage
        else : raise ValueError("Coordonnées hors matrice !")
    else :
        raise ValueError("Mauvaise période !")

def points_immerges(dictionnaire,valeur):
    """Compare les valeurs les 5 matrices présentes dans le dictionnaire de listes rendu par 'extract_csv()'
       et retourne la position de celles qui n'ont pas changer entres les 5 matrices

    Args:
        dictionnaire: dictionnaire de matrice
        valeur: 0 => jamais immergé
                1 => toujours immergé

    Raise:
        ValueError: erreur de valeur (0/1)

    Return:
        list: retourne une liste représentant (ligne,colonne)

        """
    position = []
    if valeur not in [0, 1]:
        raise ValueError("Valeur doit être de 0 ou 1")
    keys = list(dictionnaire.keys()) # Récupère toutes les clés du dictionnaire
    for i in range(len(dictionnaire[keys[0]])):
        for j in range(len(dictionnaire[keys[0]][0])):
            if all(dictionnaire[key][i][j] == valeur for key in keys): # Vérifie si la valeur est égale sur toutes les matrices
                position.append((i, j))

    return position

def print_point_immerges(liste_coord) :
    """prend une liste de tupple et l'afficher sous forme de coordonnée

    Args:
        liste_coord (list[tupple]): liste de coordonnées
    """
    for coord in liste_coord :
        print(f"Point ({coord[0]},{coord[1]})") 